(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "flexi-streams")
  (asdf:load-system "flexi-streams-test"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(unless (flexi-streams-test:run-all-tests)
  (uiop:quit 1))
